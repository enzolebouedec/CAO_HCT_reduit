#include "Geom.h"
#include "Triangulation.h"
#include <iostream>
#include <cmath>
#include <fstream>

double f(double x, double y) { return exp(x + y); };

double g(double x, double y)    { return pow(y,3) -2*x*pow(y,2) -5*pow(x,2)*y + 10*x*y +1;};
double gdx(double x, double y)  { return -2*pow(y,2) + -10*x*y + 10*y;};
double gdy(double x, double y)  { return 3*pow(y,2) + -4*x*y + -5*pow(x,2) +10*x;};

int main() {
	Triangulation ma_triang("hct.pts", "hct.tri");
	ma_triang.discretisation(0, 3, 0, 3, 100);
	
	// Fichiers de sorties
	std::ofstream res("HCT.RES");
	if (!res) {
		std::cerr << "Erreur de l'ouverture du fichier HCT.RES" << std::endl;
		return 1;
	}
	std::ofstream f_res("f.RES");
	if (!f_res) {
		std::cerr << "Erreur de l'ouverture du fichier f.RES" << std::endl;
		return 1;
	}
	std::ofstream g_res("g.RES");
	if (!g_res) {
		std::cerr << "Erreur de l'ouverture du fichier g.RES" << std::endl;
		return 1;
	}

	// Traitement pour la fonction f (exponentielle)
	ma_triang.coef_sur_elems(f, f, f);
	ma_triang.interpolant_sur_dicret();
	ma_triang.f_sur_discret(f, f, f);
	std::array<double, 2> min_max = ma_triang.min_max_fs();

	// Export dans HCT.RES
	res << "Composition de la triangulation sous la forme\nk S(1,k) S(2,k) S(3,k)" << std::endl;
	ma_triang.affiche_sommets(res);
	res << "\nOn traite ici la fonction f exponentielle\n" << std::endl;
	res << "Valeur de la fonction sur l'espace de la forme\ni x y f fdx fdy" << std::endl;
	ma_triang.affiche_valeurs_fonction(res);
	res << std::endl;
	res << "Interpolant en (2.5, 0.8) : " << ma_triang.S(2.5, 0.8) << std::endl;
	res << "Interpolant en (0.2, 1.1) : " << ma_triang.S(0.2, 1.1) << std::endl;
	res << "Interpolant en (2.9, 2.5) : " << ma_triang.S(2.9, 2.5) << std::endl;
	res << std::endl;
	res << "L'erreur minimale d'interpolation est de " << min_max[0] << std::endl;
	res << "L'erreur maximale d'interpolation est de " << min_max[1] << std::endl;
	res << std::endl;

	// Export dans f.RES
	ma_triang.affiche_valeurs_interpolant(f_res);

	// Traitement pour la gonction g (polynomiale)
	ma_triang.coef_sur_elems(g, gdx, gdy);
	ma_triang.interpolant_sur_dicret();
	ma_triang.f_sur_discret(g, gdx, gdy);
	min_max = ma_triang.min_max_fs();

	// Export dans HCT.RES
	res << "\nOn traite ici la fonction g polynomiale\n" << std::endl;
	res << "Valeur de la fonction sur l'espace de la forme\ni x y g gdx gdy" << std::endl;
	ma_triang.affiche_valeurs_fonction(res);
	res << std::endl;
	res << "Interpolant en (2.5, 0.8) : " << ma_triang.S(2.5, 0.8) << std::endl;
	res << "Interpolant en (0.2, 1.1) : " << ma_triang.S(0.2, 1.1) << std::endl;
	res << "Interpolant en (2.9, 2.5) : " << ma_triang.S(2.9, 2.5) << std::endl;
	res << std::endl;
	res << "L'erreur minimale d'interpolation est de " << min_max[0] << std::endl;
	res << "L'erreur maximale d'interpolation est de " << min_max[1] << std::endl;
	res << std::endl;

	// Export dans g.RES
	ma_triang.affiche_valeurs_interpolant(g_res);

	return 0;
}