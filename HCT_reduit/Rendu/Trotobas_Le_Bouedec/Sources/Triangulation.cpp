#include "Triangulation.h"
#include <iostream>
#include <vector>
#include "Geom.h"
#include <algorithm>
#include <cmath>

Triangulation::Triangulation(std::string nom_fichier_pts, std::string nom_fichier_triangle)
{
	// Ouverture des fichiers
	std::ifstream fichier_pts(nom_fichier_pts.c_str());
	if (!fichier_pts) {
		std::cerr << "Erreur de l'ouverture du fichier " << nom_fichier_pts << std::endl;
	}
	std::ifstream fichier_tri(nom_fichier_triangle.c_str());
	if (!fichier_tri) {
		std::cerr << "Erreur de l'ouverture du fichier " << nom_fichier_triangle << std::endl;
	}

	// Lectures des points
	int nb_pts, ID, nb_lect = 0;
	float x, y;
	
	fichier_pts >> nb_pts >> ID >> ID;
	liste_sommets.resize(nb_pts);
	
	while (fichier_pts >> ID >> x >> y) {
		liste_sommets[ID - 1] = Point(ID, x, y);
		nb_lect++;
	}
	if (nb_lect != nb_pts) {
		std::cerr << "Pas assez de points lus depuis le fichier " << nom_fichier_pts << std::endl;
		std::cerr << "Attendu : " << nb_pts << " mais seul " << nb_lect << " sont lisibles." << std::endl;
	}

	// Lecture des triangles
	int id_pt[3], id_elem = 1;
	while (fichier_tri >> id_pt[0] >> id_pt[1] >> id_pt[2])
	{
		// Ajout de l'�lement en bout avec le constructeur avec 3 pointeurs
		Point* pts[3] = { &(liste_sommets[id_pt[0] - 1]), &(liste_sommets[id_pt[1] - 1]), &(liste_sommets[id_pt[2] - 1]) };
		liste_elem.emplace_back(pts, id_elem);
		id_elem++;
	}
	
}

void Triangulation::affiche_sommets(std::ostream& os, bool affiche_coord)
{
	if (affiche_coord) {
		for (const Element& elem : liste_elem) {
			os << "Les sommets de l'�l�ment " << elem.get_id() << " sont :" << std::endl;
			for (int i = 1; i <= 3; i++) elem.get_sommet(i)->affiche();
			os << std::endl;
		}
	} else {
		for (const Element& elem : liste_elem) {
			os << elem.get_id() << '\t';
			for (Point* sommet : elem.get_sommet()) os << sommet->get_id() << '\t';
			os << std::endl;
		}
	}
}

void Triangulation::affiche_coefs(std::ostream& os)
{
	for (const Element& elem : liste_elem) {
		os << "Les coefficients de l'�l�ment " << elem.get_id() << " sont sous la forme :" << std::endl;
		os << "a1\ta2\ta3\nb1\tb2\tb3\nc1\tc2\tc3\nd1\td2\td3\ne1\te2\te3\ng1\tg2\tg3\nomega\n" << std::endl;
		int i = 0;
		for (const double coef : elem.get_coefs()) {
			std::cout << coef << '\t';
			i++;
			if (i % 3 == 0) std::cout << std::endl;
		}
		os << std::endl;
	}
}

void Triangulation::affiche_valeurs_fonction(std::ostream& os)
{
    for (Point& pt:liste_discretisation)
    {
        os << pt.get_id() << '\t' << pt.get_x() << '\t'<< pt.get_y() << '\t' << pt.get_f() << '\t' << pt.get_fdx()
           << '\t'<< pt.get_fdy() << std::endl;
    }
}

void Triangulation::affiche_valeurs_interpolant(std::ostream& os)
{
    for(Point& pt:liste_discretisation)
    {
        os << pt.get_x() << '\t' << pt.get_y() << '\t' <<pt.get_S() << std::endl;
    }
}

double Triangulation::S(double x, double y)
{
	Point P(0, x, y);
	P.find_id_elem(liste_elem);
	P.interpolant(liste_elem[P.get_id_elem() - 1].get_coefs());
	return P.get_S();
}

void Triangulation::discretisation(double xmin, double xmax, double ymin, double ymax, int npt)
{
    int counter = 1;
    const double hx = (xmax-xmin)/(npt -1);
    const double hy = (ymax-ymin)/(npt -1);
    liste_discretisation.reserve(npt*npt);
    for (int i=0 ; i<npt; i++)
    {
        for (int j=0 ; j<npt; j++)
        {
            liste_discretisation.emplace_back(counter++,xmin+i*hx,ymin+j*hy);
        }
    }
}

void Triangulation::f_sur_discret(fonction f, fonction fdx, fonction fdy)
{
    for(Point& pt:liste_discretisation) pt.calcul_fonction(std::array<fonction, 3>{f, fdx, fdy});
}

void Triangulation::coef_sur_elems(fonction f, fonction fdx, fonction fdy)
{
	for (Point& pt : liste_sommets) pt.calcul_fonction(std::array<fonction, 3>{f, fdx, fdy});
	for (Element& elem : liste_elem) elem.calcul_coef();
}

void Triangulation::interpolant_sur_dicret()
{
    for(Point& pt:liste_discretisation)
    {
        pt.find_id_elem(liste_elem);
        pt.interpolant(liste_elem[pt.get_id_elem()-1].get_coefs());
    }
}

std::array<double,2> Triangulation::min_max_fs()
{
    std::array<double,2> minmaxfms{1e20,0};
    for (int it=0 ; it<liste_discretisation.size() ; it++)
    {
        minmaxfms[0]=std::min(minmaxfms[0],std::abs(liste_discretisation[it].get_S()-liste_discretisation[it].get_f()));
        minmaxfms[1]=std::max(minmaxfms[1],std::abs(liste_discretisation[it].get_S()-liste_discretisation[it].get_f()));
    }

    return minmaxfms;

}


