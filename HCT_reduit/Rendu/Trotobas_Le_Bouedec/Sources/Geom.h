#pragma once
#include <array>
#include <vector>

typedef double(*fonction)(double x, double y);

class Point;

class Element
{
    const int id;
    std::array<double, 19> coefs;		// Coefficient pour l'élement : [a1,a2,a2, b1,b2,b3, c1,c2,c3, d1,d2,d3, e1,e2,e3, g1,g2,g3, omega]
    const std::array<Point*, 3> sommets;
	const std::array<double, 2> coord_G;// Coordonnée cartésienne du barycentre

public:
    // Constructeur
    Element(Point* pts[3], int elem_id);

	// Calcul des coefficient à partir des ffdxfdy des sommets
	void calcul_coef();

    // Getter & setter
    int get_id() const { return id; };
    Point* get_sommet(int i) const { return sommets[i - 1]; }; // Sommet numéro i
	const std::array<Point*, 3> get_sommet() const { return sommets; };
	const std::array<double, 19> get_coefs() const { return coefs; };
	const std::array<double, 2>& get_coord_G() const { return coord_G; };
};


class Point {
	int id;								// Numero du point
	int id_sous_elem;					// Valeur (1, 2 ou 3) du sous élement T_(id_sous_elem)
	int id_elem;						// id de l'un des triangle auquel appartient le point
	std::array<double, 2> coord_cart;	// Coordonnees du points
	std::array<double, 3> coord_bary;   // Coordonnées barycentriques dans le micro-triangle référencé par id_sous_elem
    std::array<double, 3> ffdxfdy;		// Valeurs de la fonction et des gradients en x et y;
    double S;                           // Valeur de l'interpolant

	// Fonction membre
	std::array<double, 3> coord_baryc(const std::array<Point*, 3>& sommets) const; // coord_bary par rapport à un triplet de point quelconque

public:
	// Constructeur
	Point() {};
	Point(int ID, double x, double y) : id{ ID }, coord_cart{ x, y } {};

	// Affichage des coordonnées cartésiennes
	void affiche() const;

	// Fonctions liées à la fonction à interpoler (appel dans cet ordre)
    void calcul_fonction(const std::array<fonction, 3>& FFDXFDY);
	double grad_directionnel (const Point& pt1, const Point& pt2) const;

	// Fonctions liées à la position dans la triangulation (appel dans cet ordre)
	void find_id_elem(const std::vector<Element>& liste_elem);
	void interpolant(const std::array<double, 19>& coefs); // A partir de coef fournis
	
	// Setter
	void set_id_elem(int ID) {id_elem = ID;};

	// Getter
	// Fonction
	double get_f() const { return ffdxfdy[0]; };
	double get_fdx() const { return ffdxfdy[1]; };
	double get_fdy() const { return ffdxfdy[2]; };
	// Coordonnées
	double get_x() const { return coord_cart[0]; };
	double get_y() const { return coord_cart[1]; };
	// Sorties
	double get_S() const { return S; };
	double get_erreur() const { return ffdxfdy[0] - S; };
	// Autre infos
	int get_id() const { return id; };
	int get_id_elem() const { return id_elem; };
};

// Produit sclaire de points connaissant leurs coord_cart
double prod_scal(const Point& A1, const Point& A2, const Point& B1, const Point& B2);