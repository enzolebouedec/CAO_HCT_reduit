#pragma once
#include <vector>
#include "Geom.h"
#include <string>
#include <fstream>

class Triangulation
{
	std::vector<Point> liste_sommets;			// NE JAMAIS RESIZE APRES LA CONSTRUCTION !!!
	std::vector<Element> liste_elem;			// Liste des éléments de la triangulation (triangles)
    std::vector<Point> liste_discretisation;	// Liste des points de la discrétisation

public:
	// Constructeur
	Triangulation(std::string nom_fichier_pts, std::string nom_fichier_triangle);

	// Afficheur
	void affiche_sommets(std::ostream& os, bool affiche_coord = false);
	void affiche_coefs(std::ostream& os);
	void affiche_valeurs_fonction(std::ostream& os);
    void affiche_valeurs_interpolant(std::ostream& os);

	// Génération de la discrétisation (appel dans cet ordre)
	void discretisation(double xmin, double xmax, double ymin, double ymax, int npt = 100);
	void coef_sur_elems(fonction f, fonction fdx, fonction fdy); // Applique f aux sommets puis calcul les coefs
	void interpolant_sur_dicret(); // Sur la grille de discretisation
	void f_sur_discret(fonction f, fonction fdx, fonction fdy);
	std::array<double, 2> min_max_fs();

	// Requiert les coefficients
	double S(double x, double y);

};

