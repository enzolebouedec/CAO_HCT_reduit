#include "Geom.h"
#include <iostream>
#include <cmath>

void Point::calcul_fonction(const std::array<fonction, 3>& FFDXFDY)
// Fonction qui a pour but de calculer les valeurs de la fonction et de ses dérivées.
{
	for (int i = 0; i < 3; i++) ffdxfdy[i] = FFDXFDY[i](coord_cart[0], coord_cart[1]);
}

double Point::grad_directionnel (const Point & pt1, const Point & pt2) const
{
	return ffdxfdy[1] * (pt2.get_x() - pt1.get_x()) + ffdxfdy[2] * (pt2.get_y() - pt1.get_y());
}

void Point::affiche() const
{
	std::cout << "Point " << id << " : (" << coord_cart[0] << ", " << coord_cart[1] << ")" << std::endl;
}

std::array<double, 3> Point::coord_baryc(const std::array<Point*, 3>& sommets) const
{
    const double x1 = sommets[0]->get_x();
    const double x2 = sommets[1]->get_x();
    const double x3 = sommets[2]->get_x();
    const double y1 = sommets[0]->get_y();
    const double y2 = sommets[1]->get_y();
    const double y3 = sommets[2]->get_y();

	std::array<double, 3> coord_bary;
	const double det = (y2-y3)*(x1-x3) + (x3-x2)*(y1-y3);
	coord_bary[0] = ((y2-y3)*(coord_cart[0]-x3) + (x3-x2)*(coord_cart[1]-y3))/det;
	coord_bary[1] = ((y3-y1)*(coord_cart[0]-x3) + (x1-x3)*(coord_cart[1]-y3))/det;
	coord_bary[2] = 1-coord_bary[0]-coord_bary[1];
	return coord_bary;
}

void Point::find_id_elem(const std::vector<Element>& liste_elem)
{
    bool isintriangle = false;
    const double epsilon = 1e-15; //Constante pour éviter les faux-négatifs !! Valeur à préciser !
	for (const Element& elem:liste_elem)
	{
		const std::array<double, 3> Coord_bar = coord_baryc(elem.get_sommet());
		if (Coord_bar[0] >=-epsilon & Coord_bar[1] >=-epsilon & Coord_bar[2]>=-epsilon)
		{
			id_elem = elem.get_id(); //MaJ de l'id du triangle auquel le point appartient

            //Test pour savoir à quel sous triangle appartient le point
            Point G(0,elem.get_coord_G()[0],elem.get_coord_G()[1]); //Creation d'un point pour le barycentre
            for (int j=1 ; j<=3 ; j++)
            {
                int k=(j+1)%4; //Respectivement j = 2,3,1
                if (k == 0){k=1;}
                const std::array<Point*,3> Ti{ {&G,elem.get_sommet(j),elem.get_sommet(k)} };
                const std::array<double, 3> Coord_bar_ST = coord_baryc(Ti);
                if (Coord_bar_ST[0] >=-epsilon & Coord_bar_ST[1] >=-epsilon & Coord_bar_ST[2]>=-epsilon)
                {
                    for (int index=0 ; index<3 ; index++){coord_bary[index]=Coord_bar_ST[index];} //MaJ des coordonnées barycentriques
                    // par rapport au sous triangle en question
                    if (k==3){id_sous_elem = 1;}
					else {id_sous_elem = k+1;}
                    isintriangle = true;
                    break;
                }
            }
            break;
		}
	}
    if (isintriangle == false)
    {
        id_elem = -99;
        std::cerr << "Attention le point (" << coord_cart[0] << "," << coord_cart[1]
                  << ") n'appartient à aucun triangles" << std::endl;
        std::cerr << "Id par défaut : - 99";
    }
}

void Point::interpolant(const std::array<double, 19>& coefs)
{
    //Permutations circulaires de 0,1,2
    const int i =id_sous_elem -1;
    const int j = (i+1)%3;
    const int k = (i+2)%3;
    //offset
    const int a = 0, b=3, c=6, d=9, e=12, g=15, omega=18;
    //Calcul de l'interpolant
    S = coefs[a+k]*pow(coord_bary[2],3) + 3*coefs[c+k]*coord_bary[1]*pow(coord_bary[2],2) +
        3*coefs[b+j]*coord_bary[2]*pow(coord_bary[1],2) + coefs[a+j]*pow(coord_bary[1],3) +
        3*coefs[d+k]*coord_bary[0]*pow(coord_bary[2],2) + 6*coefs[g+i]*coord_bary[0]*coord_bary[1]*coord_bary[2] +
        3*coefs[d+j]*coord_bary[0]*pow(coord_bary[1],2) + 3*coefs[e+k]*pow(coord_bary[0],2)*coord_bary[2] +
        3*coefs[e+j]*coord_bary[1]*pow(coord_bary[0],2) + coefs[omega]*pow(coord_bary[0],3);
}

Element::Element(Point* pts[3], int elem_id) : id{ elem_id }, sommets{ pts[0], pts[1], pts[2] }, coord_G{ (pts[0]->get_x() + pts[1]->get_x() + pts[2]->get_x()) / 3, (pts[0]->get_y() + pts[1]->get_y() + pts[2]->get_y()) / 3 }
{
	// Mise à jour de l'id_elem du point
	for (int i = 0; i < 3; i++) pts[i]->set_id_elem(elem_id);
}

void Element::calcul_coef()
{
	/* Calcul des coefficients
	Rappel de la structure de coefs
	[a1,a2,a3, b1,b2,b3, c1,c2,c3, d1,d2,d3, e1,e2,e3, g1,g2,g3, omega]
	0, 1, 2,  3, 4, 5,  6, 7, 8,  9,10,11, 12,13,14, 15,16,17,   18	<- Numéro du coefficient
	*/

	// Calcul des a
	for (int i = 0; i <= 2; i++) coefs[i] = sommets[i]->get_f();

	// Calcul des b
	coefs[3] = coefs[0] + sommets[0]->grad_directionnel(*sommets[0], *sommets[1]) / 3;
	coefs[4] = coefs[1] + sommets[1]->grad_directionnel(*sommets[1], *sommets[2]) / 3;
	coefs[5] = coefs[2] + sommets[2]->grad_directionnel(*sommets[2], *sommets[0]) / 3;

	// Calcul des c
	coefs[6] = coefs[0] + sommets[0]->grad_directionnel(*sommets[0], *sommets[2]) / 3;
	coefs[7] = coefs[1] + sommets[1]->grad_directionnel(*sommets[1], *sommets[0]) / 3;
	coefs[8] = coefs[2] + sommets[2]->grad_directionnel(*sommets[2], *sommets[1]) / 3;

	// Calcul des d
	for (int i = 0; i < 3; i++) coefs[i + 9] = (coefs[i] + coefs[i + 3] + coefs[i + 6]) / 3;

	// Calcul des g
	const Point G(0, coord_G[0], coord_G[1]); // COnstruction du barycentre à partir de ses coordonnées
	double u = 2 * prod_scal(G, *sommets[2], *sommets[1], *sommets[2]) / prod_scal(*sommets[1], *sommets[2], *sommets[1], *sommets[2]);
	coefs[15] = (2 * (coefs[10] + coefs[11]) + (4 - 3 * u)*coefs[8] + (u - 2)*coefs[2] + (3 * u - 2)*coefs[4] - u*coefs[1]) / 4;
	u = 2 * prod_scal(G, *sommets[0], *sommets[2], *sommets[0]) / prod_scal(*sommets[2], *sommets[0], *sommets[2], *sommets[0]);
	coefs[16] = (2 * (coefs[11] + coefs[9]) + (4 - 3 * u)*coefs[6] + (u - 2)*coefs[0] + (3 * u - 2)*coefs[5] - u*coefs[2]) / 4;
	u = 2 * prod_scal(G, *sommets[1], *sommets[0], *sommets[1]) / prod_scal(*sommets[0], *sommets[1], *sommets[0], *sommets[1]);
	coefs[17] = (2 * (coefs[9] + coefs[10]) + (4 - 3 * u)*coefs[7] + (u - 2)*coefs[1] + (3 * u - 2)*coefs[3] - u*coefs[0]) / 4;

	// Calcul des e
	coefs[12] = (coefs[9] + coefs[16] + coefs[17]) / 3;
	coefs[13] = (coefs[10] + coefs[15] + coefs[17]) / 3;
	coefs[14] = (coefs[11] + coefs[15] + coefs[16]) / 3;

	// Calcul de omega
	coefs[18] = (coefs[12] + coefs[13] + coefs[14]) / 3;
}

double prod_scal(const Point & A1, const Point & A2, const Point & B1, const Point & B2)
{
	// Effectue le produit scalaire de A1A2 par B1B2
	return (A2.get_x() - A1.get_x()) * (B2.get_x() - B1.get_x()) + (A2.get_y() - A1.get_y()) * (B2.get_y() - B1.get_y());
}
