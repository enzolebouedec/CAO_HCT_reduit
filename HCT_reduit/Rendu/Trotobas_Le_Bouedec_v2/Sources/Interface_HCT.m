Pts = load('hct.pts');
Pts = sortrows(Pts(2:end, :), 1);
tri = delaunay(Pts(:, 2), Pts(:, 3));

fictri = 'hct.tri';
[fid, message] = fopen(fictri,'w');
if (fid < 0) error([message,' (fichier ',fictri,')']), end
fprintf(fid,'%d %d %d\n',tri');
fclose(fid);

!g++ -std=c++11 Triangulation.cpp Geom.cpp main.cpp -o interpolant.exe
!./interpolant.exe

close all;

f = load('f.RES');
[nb_val, ~] = size(f);
nb_val = sqrt(nb_val);
f_val = reshape(f(:,3), [nb_val, nb_val]);
figure
surf(f(1:nb_val,2), f(1:nb_val,2), f_val);
title('Fonction f')
xlabel('x')
ylabel('y')

ferr = load('ferr.RES');
[nb_val, ~] = size(ferr);
nb_val = sqrt(nb_val);
ferr_val = reshape(ferr(:,3), [nb_val, nb_val]);
figure
surf(ferr(1:nb_val,2), ferr(1:nb_val,2), ferr_val);
title('Surface d erreur pour la fonction f')
xlabel('x')
ylabel('y')

g = load('g.RES');
[nb_val, ~] = size(g);
nb_val = sqrt(nb_val);
g_val = reshape(g(:,3), [nb_val, nb_val]);
figure
surf(g(1:nb_val,2), g(1:nb_val,2), g_val);
title('Fonction g')
xlabel('x')
ylabel('y')

gerr = load('gerr.RES');
[nb_val, ~] = size(gerr);
nb_val = sqrt(nb_val);
gerr_val = reshape(gerr(:,3), [nb_val, nb_val]);
figure
surf(gerr(1:nb_val,2), gerr(1:nb_val,2), gerr_val);
title('Surface d erreur pour la fonction g')
xlabel('x')
ylabel('y')